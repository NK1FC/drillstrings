function string4(obj) {
    let result = {};
    for (let key in obj) {
        let str = obj[key][0].toUpperCase() +
            obj[key].slice(1, obj[key].length).toLowerCase();
        result[key] = str;
    }
    return Object.values(result).join(' ');
}



module.exports = string4;

