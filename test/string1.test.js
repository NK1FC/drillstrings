const string1 = require('../string1.cjs');

let a = ["$100.45", "$1,002.22", "-$123", 'k'];

test('String1', () => {
    expect(string1(a)).toStrictEqual([100.45, 1002.22, -123, 0]);
});