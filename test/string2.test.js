const string2 = require('../string2.cjs');

let a = '111.11.12.1';

test('String2', () => {
    expect(string2(a)).toStrictEqual([111, 11, 12, 1]);
});