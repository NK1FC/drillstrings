function string1(str) {
    const result = str.map((curr) => {
        curr = curr.replace('$', '').replaceAll(',', '')
        if (!isNaN(curr) === true) {
            return parseFloat(curr);
        } else {
            return 0;
        }
    })
    return result;
}

module.exports = string1; 