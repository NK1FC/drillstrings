function string2(str) {
    let list = str.split('.');
    if (list.length == 4) {
        for (let num of list) {
            if (isNaN(num) === true || 256 >= parseInt(num) < 0) {
                return [];
            }
        }
        return list.map((a) => {
            return parseInt(a)
        });

    } else {
        return [];
    }

}


module.exports = string2;